# EncyclopeDIA Entrapment Analysis

This repository contains code and other content related to assessing the
behavior of entrapment analysis in EncyclopeDIA results. This work is based on
an approach that was developed and validated in our 2019 ASMS Poster "Influence
of Library Selection for Proteomics Experiments on the Accuracy of Error Rate
Estimation". We had planned to reimplement much of the approach with the goal of
automating running such an analysis, but our work has moved towards using
entrapment analysis for validation of other approaches to the issue.

The code in this repository is now being used to perform this additional
validation for our 2020 ASMS poster.