---
title: |
  | Restricting classifier training datasets improves error rate estimation when searching highly-specific libraries
  |
  | ASMS 2020 --- ThP301
author: Seth Just$^1$, Brendan MacLean$^2$, Lukas Käll$^3$, Hannes Röst$^4$ and Brian Searle$^{1,5}$
institute: $^1$Proteome Software, $^2$University of Washington, $^3$KTH Royal Institute of Technology, $^4$University of Toronto, $^5$Institute for Systems Biology
---

# **Specific** libraries

---

## **Specific** libraries

A **specific** library is one where **most targets** are present in your sample

---

## **Specific** libraries

Early proteomics techniques rely on **FASTA databases**, but most peptides won't be found.

Spectrum and chromatogram libraries are built from actual MS data, or even your exact samples.

---

## **Specific** libraries

![](img/pi0-bars){ width=75% }

Specificity is measured by $\pi_0$, or the **fraction of target peptides** that are **undetectable**.

---

Think of a proteomics experiment like a garden.

**True positive** peptides are **vegetables** and **false positives** are **weeds**.

. . .

What we care about is **how many weeds are there?** (What is $\pi_0$?)

--- 

A FASTA is like a mix of seeds, some vegetables and some weeds.

![](img/seeds-actual){ height=60% }

---

![](img/plants-mixed){ width=75% }

. . .

How many of these are weeds?

---

We need some **examples**!

![](img/seeds-decoy){ height=60% }

---

![](img/plants-comparison){ width=75% }

With a point of comparison we can **estimate** how many weeds we see.

---

![](img/plants-comparison-annot1){ width=75% }

With a large proportion of weeds this is easy

---

![](img/plants-comparison-annot2){ width=75% }

With a large proportion of weeds this is easy, and our estimate is stable.

---

With **better seeds** (a more specific library) we can reduce the number of weeds.

. . .

![](img/seeds-specific){ height=60% }

---

![](img/plants-specific){ width=75% }

Now we have a better-looking garden

---

![](img/plants-specific-annot1){ width=75% }

. . .

20%?

---

![](img/plants-specific-annot2){ width=75% }

None!?

---

![](img/plants-specific-annot2){ width=75% }

With **fewer weeds** estimating their proportion is **harder**!



# **Highly-specific** libraries affect **FDR estimation**

---

![Each step of the training process can be affected by specific searches. In the worst case this process can "run away" as overfitting lowers FDR estimates and includes more false targets in the training set.](img/training.png){ width=50% }

---

## **Restrictive training**

We can **reduce the FDR** used to **choose the training set** to ensure that
noisy FDR estimates won't allow **false targets** to be used for training.

---

## **Restrictive training** can prevent overfitting and **improve results**

![For EncyclopeDIA/Percolator, reducing training FDR from 1% to 0.01% improves quantitative accuracy](img/hexbin-encyc-two){ width=75% }



# Quantitative error drops from 0.48 to 0.36

---

## **Restrictive training** can prevent overfitting and **improve results**

Even though **intensity** is correlated with **quality**, the **ratio** we measure is totally independent!

. . .

This suggests that there was a decrease in the **true FDR**.

---

## **Restrictive training** can prevent overfitting and **improve results**

![Restrictive training gives a similar improvement in quantitative accuracy compared to entrapment](img/quant-bars-all){ width=75% }

Skyline/mProphet sees a smaller drop from 0.30 to 0.28, possibly because it uses a **pre-trained model** that's less likely to be overfit

---

## Rejecting **common errors** boosts **DIA methods**

Restrictive training **reduces** the tendency to **compress ratios** for **low-intensity** peptides.
This is a **common drawback** of DIA methods.

![](img/lfqbench-bars.png){ width=75% }

. . .

We believe this is in part due to **real signals** that are assigned to the **wrong proteome**.
By exluding these peptides from the training set, they are **correctly modeled as false**.



## Thank you

![](img/qrcode.png){ width=40% }

See our whitepaper at: [proteomesoftware.bitbucket.io/restricting-classifier-training-poster/](https://proteomesoftware.bitbucket.io/restricting-classifier-training-poster/)

---

## See our ASMS 2020 poster

[![Our poster](img/main.png "Click to download our poster"){ width=100% }](https://bitbucket.org/proteomesoftware/restricting-classifier-training-poster/downloads/restricting-classifier-training-poster.pdf)


# What if you get incredible seeds?

---

## What if you get incredible seeds?

![](img/seeds-vegetable)

---

## What if you get incredible seeds?

Plant them carefully!

. . .

![](img/plants-labeled){ width=75% }

---

## What if you get incredible seeds?

If weeds grow, you don't really care, you know where the veggies are.

![](img/plants-labeled-weeds){ width=75% }

. . .

This is like an immunoassay or targeted MS (SRM/PRM).
 
. . .

**But** this only works for a few plants!
