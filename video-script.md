Hi, and thank you for your

interest in our poster

titled "Restricting Classifier

training datasets improves

error rate estimation when

searching highly specific

libraries".



In this poster we're talking

about what we call specific

libraries.



This refers to libraries where

most of the target peptides

are present in your samples.



FASTA databases are derived

from a genome, so most of the

peptides won't be present.

When using a spectral or

chromatogram library that's

built from real mass spec data

it's much more likely that you'll

find the peptides in your sample.



We measure the specificity of

a library with a number called

pi zero, which is the fraction of

target peptides that are

undetectable.

This number varies widely between

different libraries, but in this

poster we're talking about highly

specific libraries, with less than

ten or fifteen percent pi zero.



Pi zero is something that we can

only estimate, but this can be

harder for specific libraries.



I like to think of a proteomics

experiment like a garden, where the

true positives are vegetables and

the false positives are weeds.



Estimating pi zero is just asking

"how many weeds are there"?



A FASTA database is like a big

bag of mixed seeds.



When we grow our garden we ask

"how many weeds are there?"

but there's a problem: we have

no idea what weeds look like.



So, we can get some weed seeds



and with an idea of what weeds

look like we can estimate how

many are in our garden.



When there are a lot of weeds

this is pretty easy



and we basically get the same

answer wherever we look.



If we want fewer weeds,



we can get better seeds



and our garden looks a lot better,

but when we ask how many

weeds there are



it's a little harder.



Maybe it's 20%



maybe it's none?



With more specific libraries it's

harder to estimate pi zero and FDR.



This can be complicated by

the machine learning process

that we use to learn the

difference between true

and false positives.

In some cases increasingly

specific libraries can cause 

more and more mediocre peptide

identifications to be used

as examples of true positives,

resulting in overfitting and

an underestimate of the FDR.



To resolve this issue we used

a technique we call restrictive

training where we limited the

number of peptides used to train

the machine learning model.



Analyzing a mixture of proteomes

at known ratios we saw a twenty

five percent reduction in the

root mean square error of log

ratios.



This indicates that the true FDR

was lower when we use restrictive

training.



We see a similar but much smaller

effect in Skyline, likely because

its model is pre-trained and thus 

less susceptible to overfitting.



We also see that restrictive

training reduces the tendency

to measure the wrong ratio for

low intensity peptides, which

is a common problem with DIA

methods.



This happens when a signal from

a real peptide is assigned to

a sequence from another proteome.

By preventing our model from

recognizing these identifications

we can make sure our FDR estimates

account for them.



If you'd like to learn more

follow the link to see our

whitepaper with complete results.
