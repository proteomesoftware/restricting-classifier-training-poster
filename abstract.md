# Restricting classifier training datasets improves error rate estimation when searching highly-specific libraries

(Title: 12 < 20 words)

## Authors

- Seth Just (Proteome Software) (presenting)
- Brendan MacLean (University of Washington)
- Lukas Käll (KTH Royal Institute of Technology)
- Hannes Röst (University of Toronto)
- Brian Searle (ISB, Proteome Software)

## Introduction

(118 < 120 words)

Proteomics experiments use statistics to control false discovery rates (FDRs), but estimates can vary dramatically depending on how target peptides are selected, either from spectrum libraries, or from FASTA databases. This selection considers a balance between managing error rate and comprehensive detection, where the proportion of searched target entries that are not detected (π₀) is a key measure in evaluating this balance.

Searching highly-specific libraries (low π₀) allows for relaxed error rate control and is preferable when sufficiently comprehensive libraries are available. However, many FDR estimation techniques are designed to analyze FASTA database searches with high π₀. Here we evaluate different library selections and FDR estimation parameters using DIA-MS proteomics experiments and demonstrate potential sources of uncontrolled errors.

## Methods

(110 < 120 words)

We used the Navarro 2016 datasets generated using mixtures of multiple proteomes at known ratios and a corresponding DDA spectral library of 44k peptides. We searched these datasets with Skyline (using mProphet) and EncyclopeDIA (using Percolator) to estimate π₀ and control global peptide FDR to 1%. To test the effect of low π₀ estimates, we intentionally increased the value of π₀ with "entrapment" searches using additional decoys that were labeled as targets. We also attempted to restrict false positives in the set of target PSMs used to train software classifiers by reducing the maximum allowed FDR for training from defaults (1% for Percolator and 15% for mProphet) to 0.01% ("restrictive-training").

## Preliminary data

(300 == 300 words)

FDR estimation involves generating a model that separates true target PSMs from decoys, training it to recognize features of the dataset, and using it to estimate p-values, π₀ and FDR thresholds. We hypothesize that low-π₀ searches can complicate FDR estimation through one or more mechanisms: the training process fails to choose appropriate examples of correct  peptides, the model is overfit to recognize differences between false targets and decoys, or π₀/FDR estimation produces skewed estimates.

We find that with cross-fold validation, Percolator gives good estimates of π₀ even for highly-specific searches, but is sensitive to training set selection. In these cases, low π₀ values can allow inclusion of most or all false targets in the training set (typically constructed using peptides filtered at 1% FDR in Percolator and 15% FDR in mProphet), and potentially introduce training errors. With EncyclopeDIA/Percolator we identified 35k ("normal" π₀=9.8%), 29k ("entrapment" π₀=50.7%), and 26k ("restrictive-training" π₀=19%) peptides (excluding contaminants or peptides shared between proteomes), associated with (respectively) 5350, 4039, and 4427 proteins. Comparing peptide intensities to known proteome ratios, the root-mean-square-error (RMSE) of peptide log-ratios fell from 0.60 to 0.56 and 0.45 for "entrapment" and "restrictive-training" searches respectively, indicating improved peptide signal assignment versus the "normal" search. The protein log-ratio RMSE similarly fell from 0.53 to 0.40 and 0.39. Because ratios are independent of signal intensity or PSM quality, this reduction in quantitative error suggests both approaches give lower actual FDR than the "normal" search.

With Skyline/mProphet we observe slight reduction of peptide counts and essentially equal RMSE when employing restrictive-training (31k/0.452 vs. 29k/0.446). mProphet may be less sensitive to training set selection because it does not utilize cross-fold validation, but this may make it prone to other sources of error caused by low-π₀ searches. We are continuing to explore this phenomenon within Skyline and OpenSwath.

## Novel Aspect

(18 < 20 words)

We demonstrate the diagnosis and potential solution of a class of errors in FDR estimation for DIA proteomics.
