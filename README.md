# Restricting classifier training datasets improves error rate estimation when searching highly-specific libraries

Poster for presentation at ASMS 2020, based on the work in our [2019 ASMS Poster](https://bitbucket.org/proteomesoftware/lib-fdr-2019).

See [our whitepaper](https://proteomesoftware.bitbucket.io/restricting-classifier-training-poster/) for more information and to download the full poster.

To learn more, see [http://www.proteomesoftware.com/dia](http://www.proteomesoftware.com/dia), or contact [Seth Just](mailto:seth@proteomesoftware.com).

## Copyright and License

Poster contents (text and images) are licensed [CC Attribution-NC-SA](http://creativecommons.org/licenses/by-nc-sa/3.0/).
Institution logos are copyright their respective owners and may only be used according to each institution's guidelines.

Code and other content in this repository is copyright Proteome Software Inc (2019-2020), all rights reserved unless otherwise noted.

Poster template and modifications in `main.tex` are licensed GPL-3.0, from [https://github.com/rafaelbailo/betterposter-latex-template]().
